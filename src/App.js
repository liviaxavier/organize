import React, { Component } from 'react';
// import { connect } from 'react-redux';
// import { clickButton } from './actions';
// import { bindActionCreators } from 'redux';
import DailyBudget from './features/Daily_Budget';
import Icon from '@material-ui/core/Icon';
import { Button, Modal, Paper, TextField, Input } from '@material-ui/core';

const style = {
  App: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
  },
  addItem: {
    color: "white",
    backgroundColor: "#330077",
    position: "fixed",
    bottom: "1em"
  },
  modal: {
    width: "70%",
    height: "50vh",
    margin: "auto",
    marginTop: "1em",
    padding: "2em",
    position:"relative",
    top:"15vh",
  },
  inputSize:{
    width:"100%"
  }
}

class Core extends Component {
  state = {
    open: false,
  }
  // state = {
  //   inputValue: ''
  // }
  // inputChange = event => {
  //   this.setState({
  //     inputValue: event.target.value
  //   })
  // }
  render() {
    const openModalAddItem = () => {
      this.setState({
        open: true
      })
    }
    const closeModalAddItem = () => {
      this.setState({
        open: false
      })
    }
    const createItem = () => {
      this.setState({
        open: false
      })
    }
    // const { newValue, clickButton } = this.props;
    // const { inputValue } = this.state;
    return (
      <main style={style.App}>

        <DailyBudget />
        {/* <input type='text' onChange={this.inputChange} value={inputValue} />
        <button onClick={() => clickButton(inputValue)}>
          Click me!
        </button>
        <h1>{newValue}</h1> */}
        <Button variant="fab" style={style.addItem} onClick={openModalAddItem}>
          <Icon>add</Icon>
        </Button>
        <Modal
          open={this.state.open}
          onClose={closeModalAddItem}
        >
          <Paper style={style.modal}>
            {inputs.map((input, i) => {
              return (<TextField
                style={style.inputSize}
                id={input.id}
                ket={i}
                label={input.label}
                type={input.type}
                name={input.name}
                autoComplete={input.autoComplete}
                margin={input.margin}
                variant={input.variant}
              />)
            })}

            <Button color="primary" onClick={createItem}>
              Adicionar Item
            </Button>
          </Paper>
        </Modal>
      </main >
    );
  }
}

const inputs = [
  {
    id:"area-input",
    label:"Área",
    type:"text",
    name:"area",
    margin:"normal",
    variant:"standard",
  },
  {
    id:"value-input",
    label:"Valor",
    type:"text",
    name:"value",
    margin:"normal",
    variant:"standard",
  },
  {
    id:"description-input",
    label:"Descrição",
    type:"text",
    name:"description",
    margin:"normal",
    variant:"standard",
  }
]
// const mapStateProps = store => ({
//   newValue: store.clickState.newValue
// });

// const mapDispatchToProps = dispatch =>
//   bindActionCreators({ clickButton }, dispatch);

// export default connect(mapStateProps, mapDispatchToProps)(Core);
export default Core;
