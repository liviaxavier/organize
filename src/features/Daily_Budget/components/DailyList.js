import React,
{
  Component,
  // Fragment 
} from 'react';
import {
  Paper,
  // List,
  // ListItem,
  // ListItemText,
  // ListItemSecondaryAction,
  ExpansionPanel,
  ExpansionPanelSummary,
  Typography,
  ExpansionPanelDetails,
  Avatar
} from '@material-ui/core'
// import FolderIcon from '@material-ui/icons/Folder'
import Icon from '@material-ui/core/Icon'


const style = {
  DailyList: {
  },
  textPanel: {
    alignSelf: "center",
    justifyContent: "space-between"
  },
  iconPanel: {
    marginRight: "1em"
  }
}

const list = [
  {
    area: 'Almoço',
    value: '20,50',
    description: 'Coffee Corner',
    type: 'restaurant'
  },
  {
    area: 'Open',
    value: '17,16',
    description: 'Cerveja no Japonês',
    type: 'mood'
  },
  {
    area: 'Mercado',
    value: '23,73',
    description: 'Granola e leite no Pão de açucar',
    type: 'kitchen'
  },
]



class DailyList extends Component {
  render() {
    return (
      <Paper>
        {list.map((element, i) => {
          return (
            <ExpansionPanel key={i} >
              <ExpansionPanelSummary expandIcon={<Icon>expand_more</Icon>}>
                <Avatar style={style.iconPanel}><Icon>{element.type}</Icon></Avatar>
                <Typography style={style.textPanel}>R${element.value}</Typography>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails>
                <Typography>
                  {element.description}
                </Typography>
              </ExpansionPanelDetails>
            </ExpansionPanel>
          )
        }
        )}
      </Paper>
    );
  }
}

export default DailyList;
