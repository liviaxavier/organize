import React, { Component } from 'react';
import DailyList from './components/DailyList';

const style = {
  Daily_Budget: {
    width:"90%"
  }
}

class DailyBudget extends Component {
  render() {
    return (
      <section style={style.Daily_Budget}>
        <h1>Custos diários</h1>
        <DailyList />
      </section>
    );
  }
}

export default DailyBudget;
